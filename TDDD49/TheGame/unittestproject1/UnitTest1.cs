﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TheGame.Models;
using TheGame.ViewModels;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void MovePwn()
        {
            ChessPiece pwn = null;
            ChessPiece killed = null;
            ChessBoardViewModel ChessBoardViewModel = new ChessBoardViewModel();

            foreach (ChessPiece piece in ChessBoardViewModel.ChessPieces)
            {
                if (piece.Row == 6 && piece.Column == 0)
                    pwn = piece;
                else if (piece.Row == 1 && piece.Column == 1)
                    killed = piece;
                
            }
            pwn.Move(4, 0, pwn);
            Assert.AreEqual(4, pwn.Row);

            pwn.Move(2, 0, pwn);
            Assert.AreEqual(4, pwn.Row);

            pwn.Move(3, 0, pwn);
            Assert.AreEqual(3, pwn.Row);
            Assert.AreEqual(0, pwn.Column);

            pwn.Move(2, 0, pwn);
            pwn.Move(1, 1, pwn);
            Assert.AreEqual(1, pwn.Row);
            Assert.AreEqual(1, pwn.Column);

            Assert.AreEqual(0, killed.Row);
            Assert.AreEqual(8, killed.Column);
        }

        [TestMethod]
        public void MoveRook()
        {
            ChessPiece pwn = null;
            ChessPiece rook = null;
            ChessPiece killed = null;

            ChessBoardViewModel ChessBoardViewModel = new ChessBoardViewModel();

            foreach (ChessPiece piece in ChessBoardViewModel.ChessPieces)
            {
                if (piece.Row == 6 && piece.Column == 0)
                    pwn = piece;
                else if (piece.Row == 7 && piece.Column == 0)
                    rook = piece;
                else if (piece.Row == 1 && piece.Column == 7)
                    killed = piece;
            }

            pwn.Move(4, 0, pwn);

            rook.Move(5, 0, rook);
            Assert.AreEqual(5, rook.Row);
            Assert.AreEqual(0, rook.Column);

            rook.Move(5, 7, rook);
            Assert.AreEqual(5, rook.Row);
            Assert.AreEqual(7, rook.Column);

            rook.Move(1, 7, rook);
            Assert.AreEqual(1, rook.Row);
            Assert.AreEqual(7, rook.Column);

            rook.Move(4, 3, rook);
            Assert.AreEqual(1, rook.Row);
            Assert.AreEqual(7, rook.Column);

            Assert.AreEqual(0, killed.Row);
            Assert.AreEqual(8, killed.Column);


        }

        [TestMethod]
        public void MoveKnight()
        {
            ChessPiece knight = null;
            ChessPiece killed = null;

            ChessBoardViewModel ChessBoardViewModel = new ChessBoardViewModel();

            foreach (ChessPiece piece in ChessBoardViewModel.ChessPieces)
            {
                if (piece.Row == 7 && piece.Column == 1)
                    knight = piece;
                else if (piece.Row == 1 && piece.Column == 4)
                    killed = piece;
            }

            knight.Move(6, 3, knight);
            Assert.AreEqual(7, knight.Row);
            Assert.AreEqual(1, knight.Column);

            knight.Move(5, 2, knight);
            Assert.AreEqual(5, knight.Row);
            Assert.AreEqual(2, knight.Column);

            knight.Move(3, 3, knight);
            Assert.AreEqual(3, knight.Row);
            Assert.AreEqual(3, knight.Column);

            knight.Move(1, 4, knight);
            Assert.AreEqual(1, knight.Row);
            Assert.AreEqual(4, knight.Column);

            Assert.AreEqual(0, killed.Row);
            Assert.AreEqual(8, killed.Column);

        }

        [TestMethod]
        public void MoveBishop()
        {
            ChessPiece pwn = null;
            ChessPiece bishop = null;
            ChessPiece killed = null;

            ChessBoardViewModel ChessBoardViewModel = new ChessBoardViewModel();

            foreach (ChessPiece piece in ChessBoardViewModel.ChessPieces)
            {
                if (piece.Row == 6 && piece.Column == 3)
                    pwn = piece;
                else if (piece.Row == 7 && piece.Column == 2)
                    bishop = piece;
                else if (piece.Row == 1 && piece.Column == 0)
                    killed = piece;
            }

            bishop.Move(5, 4, bishop);
            Assert.AreEqual(7, bishop.Row);
            Assert.AreEqual(2, bishop.Column);


            pwn.Move(5, 3, pwn);

            bishop.Move(5, 4, bishop);
            Assert.AreEqual(5, bishop.Row);
            Assert.AreEqual(4, bishop.Column);

            bishop.Move(1, 0, bishop);
            Assert.AreEqual(1, bishop.Row);
            Assert.AreEqual(0, bishop.Column);

            Assert.AreEqual(0, killed.Row);
            Assert.AreEqual(8, killed.Column);

        }

        [TestMethod]
        public void MoveQueen()
        {
            ChessPiece pwn = null;
            ChessPiece queen = null;
            ChessPiece killed = null;

            ChessBoardViewModel ChessBoardViewModel = new ChessBoardViewModel();

            foreach (ChessPiece piece in ChessBoardViewModel.ChessPieces)
            {
                if (piece.Row == 6 && piece.Column == 3)
                    pwn = piece;
                else if (piece.Row == 7 && piece.Column == 3)
                    queen = piece;
                else if (piece.Row == 1 && piece.Column == 3)
                    killed = piece;
            }

            pwn.Move(5,3, pwn);
            queen.Move(5, 3, queen);

            Assert.AreEqual(7, queen.Row);
            Assert.AreEqual(3, queen.Column);

            queen.Move(6, 3, queen);
            Assert.AreEqual(6, queen.Row);
            Assert.AreEqual(3, queen.Column);

            queen.Move(5, 4, queen);
            Assert.AreEqual(5, queen.Row);
            Assert.AreEqual(4, queen.Column);

            queen.Move(0, 4, queen);
            Assert.AreEqual(5, queen.Row);
            Assert.AreEqual(4, queen.Column);

            queen.Move(2, 4, queen);
            queen.Move(1, 3, queen);
            Assert.AreEqual(1, queen.Row);
            Assert.AreEqual(3, queen.Column);

            Assert.AreEqual(0, killed.Row);
            Assert.AreEqual(8, killed.Column);

        }

        [TestMethod]
        public void MoveKing()
        {
            ChessPiece pwn = null;
            ChessPiece king = null;
            ChessPiece killed = null;

            ChessBoardViewModel ChessBoardViewModel = new ChessBoardViewModel();

            foreach (ChessPiece piece in ChessBoardViewModel.ChessPieces)
            {
                if (piece.Row == 6 && piece.Column == 4)
                    pwn = piece;
                else if (piece.Row == 7 && piece.Column == 4)
                    king = piece;
                else if (piece.Row == 1 && piece.Column == 4)
                    killed = piece;
            }

            pwn.Move(5, 4, pwn);

            king.Move(6, 4, king);
            Assert.AreEqual(6, king.Row);
            Assert.AreEqual(4, king.Column);

            king.Move(4, 6, king);
            Assert.AreEqual(6, king.Row);
            Assert.AreEqual(4, king.Column);

            king.Move(5, 3, king);
            Assert.AreEqual(5, king.Row);
            Assert.AreEqual(3, king.Column);

            king.Move(4, 3, king);
            king.Move(3, 2, king);
            king.Move(2, 3, king);
            Assert.AreEqual(2, king.Row);
            Assert.AreEqual(3, king.Column);

            king.Move(0, 3, king);
            Assert.AreEqual(2, king.Row);
            Assert.AreEqual(3, king.Column);

            king.Move(1, 4, king);
            Assert.AreEqual(1, king.Row);
            Assert.AreEqual(4, king.Column);

            Assert.AreEqual(0, killed.Row);
            Assert.AreEqual(8, killed.Column);
        }

    }
}
