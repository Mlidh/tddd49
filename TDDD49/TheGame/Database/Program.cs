﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using TheGame.ViewModels;
using TheGame.Models;

namespace Database
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new ChessDatabase())
            {
                var game = new Chessboard ();
                db.Chessboards.Add(game);
                db.SaveChanges();

                var query = from g in db.Chessboards
                            orderby g.Gameid
                            select g;

                Console.WriteLine("All blogs in the database:");
                foreach (var item in query)
                {
                    Console.WriteLine(item.Gameid);
                }
            }
        }
        public class Chessboard
        {
            public int Gameid { get; set; }

            public virtual List<List<ChessPiece>> Chessboard { get; set; }
        }

        public class ChessDatabase : DbContext
        {
            public DbSet<Chessboard> Chessboards { get; set; }
        }
    }
}
