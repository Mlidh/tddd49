﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TheGame.ViewModels;
using TheGame.Models;
using TheGame.Database;

namespace TheGame.Views
{
    public partial class MainWindow : Window   
    {
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = new ChessBoardViewModel();
        }
        private ChessPiece selectedPiece;
        private void onPieceClick(object sender, MouseButtonEventArgs e)
        {
            Image clickedPiece = (Image)sender;
            string name = clickedPiece.Name;
           
            foreach(ChessPiece piece in ViewModels.ChessBoardViewModel.ChessPieces)
            {
                if(name==piece.Name)
                {
                    {
                        if (selectedPiece != null)
                        {
                            if (selectedPiece.isBlack == ChessBoardViewModel.BlkTurn)
                            {
                                selectedPiece.Move(piece.Row, piece.Column, selectedPiece);
                                selectedPiece = null;
                            }
                            else { selectedPiece = null; }
                        }
                        else { selectedPiece = piece; }
                    }
                }
            }
        }
        private void onSquareClick(object sender, MouseButtonEventArgs e)
        {
            Rectangle clickedSquare = (Rectangle)sender;
            int Row = Grid.GetRow(clickedSquare);
            int Column = Grid.GetColumn(clickedSquare);
            if (selectedPiece != null && selectedPiece.isBlack == ChessBoardViewModel.BlkTurn)
            {
                selectedPiece.Move(Row, Column, selectedPiece);
                selectedPiece = null;
            }
            else { selectedPiece = null; }        
        }
        private void newGame(object sender, RoutedEventArgs e)
        {
            ChessBoardViewModel.newGame();
        }
        private void saveGame(object sender, RoutedEventArgs e)
        {
            Program.Save(ChessBoardViewModel.ChessPieces);
        }
        private void loadGame(object sender, RoutedEventArgs e)
        {
           ChessBoardViewModel.loadGame();
        }
    }
}
