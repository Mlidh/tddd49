﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using TheGame.ViewModels;
using TheGame.Models;
using System.Windows;

namespace TheGame.Database
{
    class Program
    {
        static public void Save(List<ChessPiece> board)
        {
            using (var db = new ChessDatabase())
            {

                foreach(DbChessPiece piece in db.DbChessboard)
                {
                    db.DbChessboard.Remove(piece);
                }

                foreach(ChessPiece piece in board)
                {
                    var pi = new DbChessPiece() {Row= piece.Row, Column = piece.Column };
                    db.DbChessboard.Add(pi);
                    db.SaveChanges();
                }
            }
        }

        public class DbChessPiece
        {
            public int DbChessPieceId { get; set; }
            public int Row { get; set; }
            public int Column { get; set; }

        }

        public class ChessDatabase : DbContext
        {
            public DbSet<DbChessPiece> DbChessboard { get; set; }
        }
    }
}
