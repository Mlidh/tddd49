﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.ComponentModel;
using System.Windows;
using System.Threading.Tasks;
using TheGame.Models;
using TheGame.Commands;
using TheGame.Views;
using TheGame.Database;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Shapes;



namespace TheGame.ViewModels
{
    public class ChessBoardViewModel : INotifyPropertyChanged
    {
        static public List<ChessPiece> ChessPieces { get; set; }
        static public bool BlkTurn = false;
        public Command<ChessPiece> PieceClickCommand { get; private set; }



        public ChessBoardViewModel()
        {
            ChessPieces = new List<ChessPiece>();
            ChessPieces.Add(new Rook() { Name = "BlkRook1", Row = 0, Column = 0, isBlack = true });
            ChessPieces.Add(new Knight() { Name = "BlkKnight1", Row = 0, Column = 1, isBlack = true });
            ChessPieces.Add(new Bishop() { Name = "BlkBishop1", Row = 0, Column = 2, isBlack = true });
            ChessPieces.Add(new Queen() { Name = "BlkQueen", Row = 0, Column = 3, isBlack = true });
            ChessPieces.Add(new King() { Name = "BlkKing", Row = 0, Column = 4, isBlack = true });
            ChessPieces.Add(new Bishop() { Name = "Blkbishop2", Row = 0, Column = 5, isBlack = true });
            ChessPieces.Add(new Knight() { Name = "BlkKnight2", Row = 0, Column = 6, isBlack = true });
            ChessPieces.Add(new Rook() { Name = "BlkRook2", Row = 0, Column = 7, isBlack = true });

            ChessPieces.Add(new BlkPawn() { Name = "BlkPawn1", Row = 1, Column = 0, isBlack = true });
            ChessPieces.Add(new BlkPawn() { Name = "BlkPawn2", Row = 1, Column = 1, isBlack = true });
            ChessPieces.Add(new BlkPawn() { Name = "BlkPawn3", Row = 1, Column = 2, isBlack = true });
            ChessPieces.Add(new BlkPawn() { Name = "BlkPawn4", Row = 1, Column = 3, isBlack = true });
            ChessPieces.Add(new BlkPawn() { Name = "BlkPawn5", Row = 1, Column = 4, isBlack = true });
            ChessPieces.Add(new BlkPawn() { Name = "BlkPawn6", Row = 1, Column = 5, isBlack = true });
            ChessPieces.Add(new BlkPawn() { Name = "BlkPawn7", Row = 1, Column = 6, isBlack = true });
            ChessPieces.Add(new BlkPawn() { Name = "BlkPawn8", Row = 1, Column = 7, isBlack = true });

            ChessPieces.Add(new Rook() { Name = "WhtRook1", Row = 7, Column = 0, isBlack = false });
            ChessPieces.Add(new Knight() { Name = "WhtKnight1", Row = 7, Column = 1, isBlack = false });
            ChessPieces.Add(new Bishop() { Name = "WhtBishop1", Row = 7, Column = 2, isBlack = false });
            ChessPieces.Add(new Queen() { Name = "WhtQueen", Row = 7, Column = 3, isBlack = false });
            ChessPieces.Add(new King() { Name = "WhtKing", Row = 7, Column = 4, isBlack = false });
            ChessPieces.Add(new Bishop() { Name = "WhtBishop2", Row = 7, Column = 5, isBlack = false });
            ChessPieces.Add(new Knight() { Name = "WhtKnight2", Row = 7, Column = 6, isBlack = false });
            ChessPieces.Add(new Rook() { Name = "WhtRook2", Row = 7, Column = 7, isBlack = false });

            ChessPieces.Add(new WhtPawn() { Name = "WhtPawn1", Row = 6, Column = 0, isBlack = false });
            ChessPieces.Add(new WhtPawn() { Name = "WhtPawn2", Row = 6, Column = 1, isBlack = false });
            ChessPieces.Add(new WhtPawn() { Name = "WhtPawn3", Row = 6, Column = 2, isBlack = false });
            ChessPieces.Add(new WhtPawn() { Name = "WhtPawn4", Row = 6, Column = 3, isBlack = false });
            ChessPieces.Add(new WhtPawn() { Name = "WhtPawn5", Row = 6, Column = 4, isBlack = false });
            ChessPieces.Add(new WhtPawn() { Name = "WhtPawn6", Row = 6, Column = 5, isBlack = false });
            ChessPieces.Add(new WhtPawn() { Name = "WhtPawn7", Row = 6, Column = 6, isBlack = false });
            ChessPieces.Add(new WhtPawn() { Name = "WhtPawn8", Row = 6, Column = 7, isBlack = false });

        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }



        internal static void newGame()
        {
            foreach (ChessPiece piece in ChessPieces)
            {
                if (piece.Name.Contains("WhtPawn"))
                {
                    WhtPawn pawn = (WhtPawn)piece;
                    pawn.firstMove = true;
                }
                else if (piece.Name.Contains("BlkPawn"))
                {
                    BlkPawn pawn = (BlkPawn)piece;
                    pawn.firstMove = true;
                }
                piece.Row = piece.startRow;
                piece.Column = piece.startColumn;
                BlkTurn = false;
            }
        }
        internal static void loadGame()
        {
            using (var db = new Program.ChessDatabase())
            {
                int pieceIndex = 0;
                foreach (Program.DbChessPiece piece in db.DbChessboard)
                {
                    ChessPieces[pieceIndex].Row = piece.Row;
                    ChessPieces[pieceIndex].Column = piece.Column;
                    pieceIndex++;
                }
            }
        }
    }
}
