﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Controls;

namespace TheGame.Models
{
    public class ChessPiece : INotifyPropertyChanged
    {
        public String Name { get; set; }
        private int _Row, _Column;
        public int _startRow = -1, _startColumn = -1;
        public bool isBlack;
        
        public int startRow
        {
            get { return _startRow; }
            set
            {
                _startRow = value;
                NotifyPropertyChanged("startRow");
            }


        }
        public int startColumn
        {
            get { return _startColumn; }
            set
            {
                _startColumn = value;
                NotifyPropertyChanged("startColumn");
            }

        }
        public int Row
        {
            get { return _Row; }
            set
            {
                _Row = value;
                NotifyPropertyChanged("Row");
                if (_startRow == -1)
                    _startRow = value;
            }

        }

        public int Column
        {
            get { return _Column; }
            set
            {
                _Column = value;
                NotifyPropertyChanged("Column");
                if (_startColumn == -1)
                    _startColumn = value;
            }
        }
        public virtual void Move(int row,int column, ChessPiece selectedPiece)
        {
            
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        
    }
}
