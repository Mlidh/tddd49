﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using TheGame.ViewModels;

namespace TheGame.Models
{
    public class Knight: ChessPiece
    {
        public override void Move(int row, int column, ChessPiece selectedPiece)
        {
            bool validMove = true;
            ChessPiece killedPiece = null;

            if( ( Math.Abs(selectedPiece.Row - row) == 2 && Math.Abs(selectedPiece.Column - column ) == 1 ) ||
                ( Math.Abs(selectedPiece.Column - column) == 2 && Math.Abs(selectedPiece.Row - row) == 1 ))
            {

                foreach (ChessPiece piece in ChessBoardViewModel.ChessPieces)
                {
                    if (piece.Row == row && piece.Column == column)
                    {
                        if (piece.isBlack != selectedPiece.isBlack)
                        {
                            killedPiece = piece;
                        }
                        else
                        {
                            validMove = false;
                        }
                    }
                }

                if (killedPiece != null && validMove)
                {
                    if (killedPiece.isBlack == true)
                    {
                        killedPiece.Row = 0;
                        killedPiece.Column = 8;
                        selectedPiece.Row = row;
                        selectedPiece.Column = column;
                        ChessBoardViewModel.BlkTurn = !ChessBoardViewModel.BlkTurn;
                        if (killedPiece.Name == "BlkKing")
                        {
                            MessageBox.Show("White team is victorious!");
                        }

                    }
                    else
                    {
                        killedPiece.Row = 8;
                        killedPiece.Column = 8;
                        selectedPiece.Row = row;
                        selectedPiece.Column = column;
                        ChessBoardViewModel.BlkTurn = !ChessBoardViewModel.BlkTurn;
                        if (killedPiece.Name == "WhtKing")
                        {
                            MessageBox.Show("Black team is victorious!");
                        }
                    }

                }
                else if (validMove)
                {
                    selectedPiece.Row = row;
                    selectedPiece.Column = column;
                    ChessBoardViewModel.BlkTurn = !ChessBoardViewModel.BlkTurn;
                }
            }
        }
    }
}
