﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TheGame.ViewModels;
using System.Windows;

namespace TheGame.Models
{
    public class Queen : ChessPiece
    {
        public override void Move(int row, int column, ChessPiece selectedPiece)
        {
            int selectedRow = selectedPiece.Row;
            int selectedColumn = selectedPiece.Column;
            bool validMove = true;
            ChessPiece killedPiece = null;
            if (selectedPiece.Column == column)
            {
                if (selectedPiece.Row > row)
                {
                    foreach (ChessPiece piece in ChessBoardViewModel.ChessPieces)
                    {
                        if (piece.Column == selectedPiece.Column && piece.Row < selectedPiece.Row && piece.Row > row)
                        {
                            validMove = false;
                            killedPiece = null;
                        }
                        else if (selectedPiece.Column == piece.Column && piece.Row == row && validMove)
                        {
                            if (piece.isBlack != selectedPiece.isBlack)
                            {
                                killedPiece = piece;
                            }
                            else
                            {
                                validMove = false;
                                killedPiece = null;
                            }
                        }

                    }

                }
                else if (selectedPiece.Row < row)
                {
                    foreach (ChessPiece piece in ChessBoardViewModel.ChessPieces)
                    {
                        if (piece.Column == selectedPiece.Column && piece.Row > selectedPiece.Row && piece.Row < row)
                        {
                            validMove = false;
                            killedPiece = null;
                        }
                        else if (selectedPiece.Column == piece.Column && piece.Row == row && validMove)
                        {
                            if (piece.isBlack != selectedPiece.isBlack)
                            {
                                killedPiece = piece;
                            }
                            else
                            {
                                validMove = false;
                                killedPiece = null;
                            }
                        }
                    }
                }




            }
            else if (selectedPiece.Row == row)
            {
                if (selectedPiece.Column > column)
                {
                    foreach (ChessPiece piece in ChessBoardViewModel.ChessPieces)
                    {
                        if (piece.Row == selectedPiece.Row && piece.Column < selectedPiece.Column && piece.Column > column)
                        {
                            validMove = false;
                            killedPiece = null;
                        }
                        else if (selectedPiece.Row == piece.Row && piece.Column == column && validMove)
                        {
                            if (piece.isBlack != selectedPiece.isBlack)
                            {
                                killedPiece = piece;
                            }
                            else
                            {
                                validMove = false;
                                killedPiece = null;
                            }
                        }
                    }

                }
                else if (selectedPiece.Column < column)
                {
                    foreach (ChessPiece piece in ChessBoardViewModel.ChessPieces)
                    {
                        if (piece.Row == selectedPiece.Row && piece.Column > selectedPiece.Column && piece.Column < column)
                        {
                            validMove = false;
                            killedPiece = null;
                        }
                        else if (selectedPiece.Row == piece.Row && piece.Column == column && validMove)
                        {
                            if (piece.isBlack != selectedPiece.isBlack)
                            {
                                killedPiece = piece;
                            }
                            else
                            {
                                validMove = false;
                                killedPiece = null;
                            }
                        }
                    }
                }
            }
            if (selectedPiece.Row < row && selectedPiece.Column < column)
            {
                while (selectedRow != (row + 1))
                {
                    foreach (ChessPiece piece in ChessBoardViewModel.ChessPieces)
                    {
                        if (piece.Column == selectedColumn && piece.Row == selectedRow && selectedPiece.Name != piece.Name)
                        {
                            if (piece.isBlack != selectedPiece.isBlack && piece.Row == row && piece.Column == column)
                            {
                                killedPiece = piece;
                            }
                            else
                            {
                                validMove = false;
                                killedPiece = null;
                            }
                        }
                    }
                    selectedRow += 1;
                    selectedColumn += 1;

                }
            }
            if (selectedPiece.Row < row && selectedPiece.Column > column)
            {
                while (selectedRow != (row + 1))
                {
                    foreach (ChessPiece piece in ChessBoardViewModel.ChessPieces)
                    {
                        if (piece.Column == selectedColumn && piece.Row == selectedRow && selectedPiece.Name != piece.Name)
                        {
                            if (piece.isBlack != selectedPiece.isBlack && piece.Row == row && piece.Column == column)
                            {
                                killedPiece = piece;
                            }
                            else
                            {
                                validMove = false;
                                killedPiece = null;
                            }
                        }
                    }
                    selectedRow += 1;
                    selectedColumn -= 1;

                }
            }
            if (selectedPiece.Row > row && selectedPiece.Column < column)
            {
                while (selectedRow != (row - 1))
                {
                    foreach (ChessPiece piece in ChessBoardViewModel.ChessPieces)
                    {
                        if (piece.Column == selectedColumn && piece.Row == selectedRow && selectedPiece.Name != piece.Name)
                        {
                            if (piece.isBlack != selectedPiece.isBlack && piece.Row == row && piece.Column == column)
                            {
                                killedPiece = piece;
                            }
                            else
                            {
                                validMove = false;
                                killedPiece = null;
                            }
                        }
                    }
                    selectedRow -= 1;
                    selectedColumn += 1;

                }
            }
            if (selectedPiece.Row > row && selectedPiece.Column > column)
            {
                while (selectedRow != (row - 1))
                {
                    foreach (ChessPiece piece in ChessBoardViewModel.ChessPieces)
                    {
                        if (piece.Column == selectedColumn && piece.Row == selectedRow && selectedPiece.Name != piece.Name)
                        {
                            if (piece.isBlack != selectedPiece.isBlack && piece.Row == row && piece.Column == column)
                            {
                                killedPiece = piece;
                            }
                            else
                            {
                                validMove = false;
                                killedPiece = null;
                            }
                        }
                    }
                    selectedRow -= 1;
                    selectedColumn -= 1;

                }
            }
            if (killedPiece != null && validMove)
            {
                if (killedPiece.isBlack == true)
                {
                    killedPiece.Row = 0;
                    killedPiece.Column = 8;
                    selectedPiece.Row = row;
                    selectedPiece.Column = column;
                    ChessBoardViewModel.BlkTurn = !ChessBoardViewModel.BlkTurn;
                    if (killedPiece.Name == "BlkKing")
                    {
                        MessageBox.Show("White team is victorious!");
                    }

                }
                else
                {
                    killedPiece.Row = 8;
                    killedPiece.Column = 8;
                    selectedPiece.Row = row;
                    selectedPiece.Column = column;
                    ChessBoardViewModel.BlkTurn = !ChessBoardViewModel.BlkTurn;
                    if (killedPiece.Name == "WhtKing")
                    {
                        MessageBox.Show("Black team is victorious!");
                    }
                }

            }
            else if (validMove)
            {
                selectedPiece.Row = row;
                selectedPiece.Column = column;
                ChessBoardViewModel.BlkTurn = !ChessBoardViewModel.BlkTurn;
            }
        }

    }
}
