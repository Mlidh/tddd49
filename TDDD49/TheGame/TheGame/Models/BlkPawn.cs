﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TheGame.ViewModels;

namespace TheGame.Models
{
    public class BlkPawn : ChessPiece
    {
        public bool firstMove = true;
        bool validMove;
        public override void Move(int row, int column, ChessPiece selectedPiece)
        {
            validMove = true;
            if (selectedPiece.Column == (column - 1) || selectedPiece.Column == (column + 1))
            {
                if (row == (selectedPiece.Row + 2) && firstMove)
                {
                    foreach (ChessPiece piece in ChessBoardViewModel.ChessPieces)
                    {
                        if (piece.Row == row && piece.Column == column && piece.isBlack == false)
                        {
                            piece.Row = 8;
                            piece.Column = 8;
                            firstMove = false;
                            selectedPiece.Row = row;
                            selectedPiece.Column = column;
                            ChessBoardViewModel.BlkTurn = !ChessBoardViewModel.BlkTurn;
                            if (piece.Name == "WhtKing")
                            {
                                MessageBox.Show("Black team is victorious!");
                            }

                        }

                    }
                }
                else if (row == (selectedPiece.Row + 1))
                {
                    foreach (ChessPiece piece in ChessBoardViewModel.ChessPieces)
                    {
                        if (piece.Row == row && piece.Column == column && piece.isBlack == false)
                        {
                            piece.Row = 8;
                            piece.Column = 8;
                            firstMove = false;
                            selectedPiece.Row = row;
                            selectedPiece.Column = column;
                            ChessBoardViewModel.BlkTurn = !ChessBoardViewModel.BlkTurn;
                            if (piece.Name == "WhtKing")
                            {
                                MessageBox.Show("Black team is victorious!");
                            }

                        }

                    }
                }
                else { validMove = false; }
            }
            else if (selectedPiece.Column == column)
            {
                if (row == (selectedPiece.Row + 2) && firstMove)
                {
                    foreach (ChessPiece piece in ChessBoardViewModel.ChessPieces)
                    {
                        if (piece.Column == column && piece.Row == (row))
                        {
                            validMove = false;
                        }
                    }
                }
                else if (row == (selectedPiece.Row + 1))
                {
                    foreach (ChessPiece piece in ChessBoardViewModel.ChessPieces)
                    {
                        if (piece.Column == column && piece.Row == (row))
                        {
                            validMove = false;
                        }
                    }
                }
                else { validMove = false; }
                if (validMove)
                {
                    firstMove = false;
                    selectedPiece.Row = row;
                    selectedPiece.Column = column;
                    ChessBoardViewModel.BlkTurn = !ChessBoardViewModel.BlkTurn;
                }
            }
        }
    }
}
